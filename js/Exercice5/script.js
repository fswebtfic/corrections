$(function()
{
    popup();
});

this.popup = function()
{
    let xOffset = 10;
    let yOffset = 20;
    $("a.infobulle").on("mouseenter",
                    function(e)
                    {
                        this.texte = this.title;
                        this.title = "";

                        $("body").append("<p id='infobulle'>"+ this.texte +"</p>");
                        $("#infobulle")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px")
                        .fadeIn("fast");
                    });
    $("a.infobulle").on("mouseleave",
                    function()
                    {
                        this.title = this.texte;
                        $("#infobulle").remove();
                    });

                    
    $("a.infobulle").mousemove(
                    function(e)
                    {
                        $("#infobulle")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px");
                    });
    };
    
 