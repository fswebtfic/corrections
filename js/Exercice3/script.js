$(function(){
	
	//Cacher tous les contenus
	$(".contenu").hide();
	
	//événement mouseover sur les onglets
	$(".onglet").on("hover",function() {
		//On remet le style normal aux onglets
		$(".onglet").css("background-Color","#CCC");
		$(".onglet").css("color","black");
		
		//On change le style de l'élément courant
		//event.target = $(this)
		$(this).css("background-Color","#FFF");
		$(this).css("color","blue");
		
		//on remet le style à l'élément correspondant au contenu
		var element =$(".contenu:visible").attr("id");
		if(!element)//element == undefined
		{
			element =$(".contenuV:visible").attr("id");			
		}
		
		if(element)
		{
			var idContenu = element.replace("contenu","");
			if(idContenu!="undefined")
			{
				$("#Onglet"+idContenu).css("background-Color","#FFF");
				$("#Onglet"+idContenu).css("color","blue");
			}
		}
		
		
		
	});
	
	//événement click sur les onglets
	$(".onglet").on("click",function(){
		var idO= $(this).attr("id").replace("Onglet","");
		
		//On remet le style normal aux onglets
		$(".onglet").css("background-Color","#CCC");
		$(".onglet").css("color","black");
		
		//On change le style de l'éléement courant
		$(this).css("background-Color","#FFF");
		$(this).css("color","blue");
		
		//Cacher tous les contenus
		$(".contenu").hide();
		$(".contenuV").hide();
		//Afficher le bon
		$("#contenu"+idO).show();
	});
	
	//événement click sur le bouton
	$("#Change").on("click",function(){
		
		var btnContenu=$(this).val();
		if(btnContenu=="Vertical")
		{
			//contenuV
			$(".onglet").css("float","none");
			$(".contenu").addClass("contenuV");
			$(".contenuV").removeClass("contenu");			
			$(this).val("Horizontal");
		}
		else
		{
			//contenu
			$(".onglet").css("float","left");			
			$(".contenuV").addClass("contenu");
			$(".contenu").removeClass("contenuV")
			$(this).val("Vertical");
		}
		
		//on remet le style à l'élément correspondant au contenu
		var el =$(".contenu:visible").attr("id");
		if(el != undefined)
		{
			
		var idContenu = el.replace("contenu","");
		
			$("#Onglet"+idContenu).css("background-Color","#FFF");
			$("#Onglet"+idContenu).css("color","blue");
		}
		
	});

});