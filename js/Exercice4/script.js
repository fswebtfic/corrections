$(function()
{
    zoom_image();
}); 

let zoom_image = 
            function()
            {
                    //Variable permettant de stocker le décalage pour l'affichage
                    let xOffset = 10;
                    let yOffset = 30;
                    //Mes événements
                    $("a.zoom").on("mouseenter",(e)=>fnIn(e,xOffset,yOffset)); 
                    $("a.zoom").on("mouseleave",(e)=>fnOut(e,xOffset,yOffset));
                    $("a.zoom").on("mousemove",(e)=>zoom(e,xOffset,yOffset));
            };
    

let fnIn=function(e, xOffset,yOffset) 
            {
                //L'événement e cible l'image
                // donc je récupère la target et ensuite sont parent a
                let targetA = $(e.target).parent("a")[0];
               
                let legende = (targetA.title != "") ? "<br/>" + targetA.title : "";
                $("body").append("<p id='zoom'><img src='"+ targetA.href +"'alt='Visualisation image' />"+ legende +"</p>");
                $("#zoom")
                        .css("top",(e.pageY - xOffset) + "px")
                        .css("left",(e.pageX + yOffset) + "px")
                        .fadeIn("slow");

                        
            }

 let fnOut=function (e, xOffset,yOffset)
            {
                 //L'événement e cible l'image
                // donc je récupère la target et ensuite sont parent a
                let targetA = $(e.target).parent("a")[0];  
                $("#zoom").fadeOut("slow").remove();
            }

 let zoom =  function (e, xOffset,yOffset)
            { 
                $("#zoom")
                    .css("top",(e.pageY - xOffset) + "px")
                    .css("left",(e.pageX + yOffset) + "px"); 
            }