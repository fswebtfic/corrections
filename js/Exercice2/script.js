let taille = 16;

$(function()
{
    $("#btnPlus").on("click", Increase);
    $("#btnMoins").on("click", Decrease);

});

function getCurrentSize()
{
    let fontSize =$("#Texte").css("font-size"); //16px
    if(fontSize != undefined)
    {
        taille= parseInt(fontSize);
    }
    
}

function Increase()
{
    getCurrentSize();
    taille++;
    //$("#Texte").css({ fontSize: taille+"px"});
    $("#Texte").css("font-size",taille+"px" );
}

function Decrease()
{
    getCurrentSize();
    taille--;
    if(taille<0) taille = 0;

    $("#Texte").css("font-size",taille+"px" );
}